#!/bin/sh

echo "background offset: 0"
./pcx2sprite bg.pcx > bg.spr
./rle bg.spr dojob > ../sudoku86.dat
rm bg.spr

echo "palette offset...: `stat -c '%s' ../sudoku86.dat`"
./pcx2pal palette.pcx >> ../sudoku86.dat

echo "done img offset..: `stat -c '%s' ../sudoku86.dat`"
./pcx2sprite done.pcx >> done.spr
./rle done.spr dojob >> ../sudoku86.dat
rm done.spr
