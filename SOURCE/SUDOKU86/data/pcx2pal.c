#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  unsigned char pal[720];
  int x;
  FILE *fd;

  if (argc != 2) {
    puts("Usage: pcx2pal file.pcx");
    return(1);
  }

  fd = fopen(argv[1], "rb");
  if (fd == NULL) return(1);

  fseek(fd, -720, SEEK_END);
  fread(pal, 1, 720, fd);
  fclose(fd);

  for (x = 0; x < 720; x++) printf("%c", pal[x] >> 2); /* divide by 4 because VGA expects a range of 0..63 */

  return(0);
}
