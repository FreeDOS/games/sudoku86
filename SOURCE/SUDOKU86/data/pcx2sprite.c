#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  int x, pcxwidth, pcxheight, pcxbpp, bytebuff, rle;
  long pixelsdumped = 0, pos;
  unsigned char *pcx;
  FILE *fd;

  if (argc != 2) {
    puts("Usage: pcx2sprite file.pcx");
    return(1);
  }

  fd = fopen(argv[1], "rb");
  if (fd == NULL) return(1);

  pcx = malloc(4*1024*1024);

  fread(pcx, 1, 4*1024*1024, fd);
  fclose(fd);
  
  /* check that this is a pcx file */
  if ((pcx[0] != 10) || (pcx[1] != 5) || (pcx[65] != 1)) {
    free(pcx);
    return(2);
  }

  pcxbpp = pcx[3];
  pcxwidth = 1 + ((pcx[9] << 8) | pcx[8]) - ((pcx[5] << 8) | pcx[4]);
  pcxheight = 1 + ((pcx[11] << 8) | pcx[10]) - ((pcx[7] << 8) | pcx[6]);;

  pos = 128;
  while (pixelsdumped < pcxwidth * pcxheight) {
    bytebuff = pcx[pos++];
    if ((bytebuff & 192) == 192) { /* this is a RLE value */
        rle = bytebuff & 63;
        bytebuff = pcx[pos++];
      } else { /* this is pixel data */
        rle = 1;
    }
    for (; rle > 0; rle--) {
      printf("%c", bytebuff);
      pixelsdumped++;
    }
  }

  free(pcx);

  return(0);
}
