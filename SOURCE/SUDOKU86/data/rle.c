/*
 * RLE compressor
 * Copyright (C) Mateusz Viste 2014
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAXFILE 8*1024*1024

/* compress a file and return the compressed size */
long rlecomp(unsigned char *file, long filesize, int rlebitlen, int dojob) {
    int rle = 0, x, rlemaxval;
    long outsize = 1;
    unsigned char lastbyte, rlebitmask;
    rlebitmask = (255 << rlebitlen) & 0xFF;
    rlemaxval = 1 << rlebitlen;
    if (dojob != 0) printf("%c", rlebitlen);
    lastbyte = file[0];
    for (x = 0; x <= filesize; x++) {
      if ((file[x] == lastbyte) && (rle < rlemaxval) && (x < filesize)) {
          rle += 1;
        } else { /* dump last RLE chunk */
          if ((rle == 1) && ((lastbyte & rlebitmask) != rlebitmask)) { /* no need to write a RLE marker */
              if (dojob != 0) printf("%c", lastbyte);
              outsize += 1;
            } else {
              if (dojob != 0) printf("%c%c", (rle - 1) | rlebitmask, lastbyte);
              outsize += 2;
          }
          lastbyte = file[x];
          rle = 1;
      }
    }
    return(outsize);
}


int main(int argc, char **argv) {
  int rlebitlen, bestrle = 0, dojob = 0;
  unsigned char *file;
  long filesize, outsize, bestresult = 0;
  char *fileout = NULL;
  FILE *fd;

  if ((argc < 2) || (argc > 3)) {
    puts("Usage: rle file [dojob]");
    return(0);
  }
  if (argc == 3) {
    if (strcmp(argv[2], "dojob") == 0) dojob = 1;
  }

  fd = fopen(argv[1], "rb");
  if (fd == NULL) {
    puts("Unable to open file");
    return(0);
  }

  file = malloc(MAXFILE);

  if (file == NULL) {
    puts("Out of mem!");
    fclose(fd);
    return(0);
  }

  filesize = fread(file, 1, MAXFILE, fd);
  fclose(fd);

  for (rlebitlen = 1; rlebitlen < 8; rlebitlen++) {

    outsize = rlecomp(file, filesize, rlebitlen, 0);
    if ((outsize < bestresult) || (bestrle == 0)) {
      bestresult = outsize;
      bestrle = rlebitlen;
    }
    if (dojob == 0) printf("RLE BITLEN %d: %ld -> %ld (%0.2Lf%%)\n", rlebitlen, filesize, outsize, (long double)(outsize * 100) / filesize - 100);

  }
  if (dojob == 0) printf("\nBEST RATIO %d: %ld -> %ld (%0.2Lf%%)\n", bestrle, filesize, bestresult, (long double)(bestresult * 100) / filesize - 100);

  if (dojob != 0) rlecomp(file, filesize, bestrle, dojob);

  return(0);
}
