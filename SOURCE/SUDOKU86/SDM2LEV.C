/*
 * sdm2lev -- converts SDM collections to Sudoku86 level format.
 * Copyright (C) 2014 Mateusz Viste
 */

#include <stdio.h>
#include <string.h>

int getnextline(FILE *fd, char *buff, int maxlen) {
  int bytebuff, cursor = 0, res = 0;
  for (;;) {
    if (cursor >= maxlen) return(1);
    bytebuff = fgetc(fd);
    if (bytebuff < 0) {
      res = 1;
      bytebuff = '\n';
    }
    switch (bytebuff) {
      case '\r':
        break;
      case '\n':
        buff[cursor] = 0;
        return(res);
      default:
        buff[cursor++] = bytebuff;
        break;
    }
  }
}

void sdm2lev(char *txt, unsigned char *lev) {
  int x;
  unsigned char sdm[82];
  /* transform txt to binary */
  for (x = 0; x < 81; x++) {
    if ((txt[x] >= '1') && (txt[x] <= '9')) {
        sdm[x] = txt[x] - '0';
      } else {
        sdm[x] = 0;
    }
  }
  sdm[81] = 0;
  /* pack binary */
  for (x = 0; x < 81; x += 2) {
    lev[x >> 1] = sdm[x];
    lev[x >> 1] <<= 4;
    lev[x >> 1] |= sdm[x + 1];
  }
}

int main(int argc, char **argv) {
  char linebuff[90];
  char unsigned sdmbuff[90];
  int x;
  long sdmcount = 0;
  FILE *fd, *fdout;

  if (argc != 3) {
    puts("sdm2lev -- converts sdm collections to Sudoku86 level format\n"
         "Copyright (C) 2014 Mateusz Viste\n"
         "\n"
         "Usage: sdm2lev file.sdm file.lev\n");
    return(1);
  }

  fd = fopen(argv[1], "rb");
  if (fd == NULL) return(1);
  fdout = fopen(argv[2], "wb");
  if (fdout == NULL) {
    puts("Error: failed to open output file");
    fclose(fd);
    return(1);
  }

  for (;;) {
    /* read a line */
    if (getnextline(fd, linebuff, 85) != 0) break;
    /* if line != 81 chars, quit */
    if (strlen(linebuff) != 81) {
      printf("Error: invalid file format (%d)\n", strlen(linebuff));
      break;
    }
    /* convert line into lev data */
    sdm2lev(linebuff, sdmbuff);
    /* print out */
    for (x = 0; x < 41; x++) fprintf(fdout, "%c", sdmbuff[x]);
    sdmcount += 1;
  }

  fclose(fdout);
  fclose(fd);

  printf("Processed %d levels.\n", sdmcount);

  return(0);
}
